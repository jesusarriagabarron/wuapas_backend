<?php
/**
 * Created by PhpStorm.
 * User: jbarron
 * Date: 4/3/17
 * Time: 10:54 PM
 */

namespace App\Http\Controllers\AppConfig;


use App\Http\Controllers\Controller;
use App\MainMenu;
use Illuminate\Http\Request;

class MenuController extends Controller
{

	public function getAppMenu(Request $request)
	{
		sleep(1);
		$menu = MainMenu::all();
		return response()->json($menu,200);
	}
	
}
