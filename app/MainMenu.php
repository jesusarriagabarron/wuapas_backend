<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{

	protected $table = "main_menu";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title','description','image','icon','enabled'
	];



}
